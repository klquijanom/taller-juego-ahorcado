let letras= ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];
let palabras=["perro", "gato", "cocina", "casa", "universidad","familia", "herramienta", "nombre"];
let c;
c=Math.round(Math.random()*8);     
let k=0;
let errores=0;
let aciertos=0;


class Ahorcado{
    palabras;

    constructor(palabras){
        this.palabras=palabras;
        this.jugar();
    }

    jugar(){
        this.hacer_teclado();
        this.escojer_palabra();  
    }

    hacer_teclado(){
        let teclado = document.getElementById("divTeclado");
        for(let i=0; i<letras.length;i++){
            teclado.innerHTML = teclado.innerHTML + "<input type='button' id='letraT"+(i+1)+"' class='teclas' value='"+ (letras[i]) +"' onclick='miAhorcado.verificarLetra()'>";
        }    
    }
    escojer_palabra(){   
        let palabraArreglo = palabras[c]; 
        let palabraDeJuego = palabraArreglo.split('');

        let division = document.getElementById("palabra");

        if(k<1){
        for(let i=0; i<palabraArreglo.length;i++){
            division.innerHTML = division.innerHTML + "<input type='button' class='inputs' id='letraP"+(i)+"'>";
        }
        k++;
    }
        return(palabraDeJuego);
}

    verificarLetra(){
        let juego = this.escojer_palabra();

        let miTecla;
        miTecla = event.target;  

        let verificar = palabras[c].indexOf(miTecla.value);
        if(verificar===-1){
            errores++;
            let imagen = new Image();
        if(errores===1){
            imagen.src= "ahorcadoCabeza.jpg"
            imagen.onload = function(){
                ctx.drawImage(imagen, 40, 15);
            }
        }
            if(errores===2){
                imagen.src= "ahorcadoCuerpo.jpg"
                imagen.onload = function(){
                    ctx.drawImage(imagen, 40, 15);
                }
            }
                if(errores===3){
                    imagen.src= "mano1.jpg"
                    imagen.onload = function(){
                        ctx.drawImage(imagen, 40, 15);
                    }
                }
                    if(errores===4){
                        imagen.src= "mano2.jpg"
                        imagen.onload = function(){
                            ctx.drawImage(imagen, 40, 15);
                        }
                    }
                        if(errores===5){
                            imagen.src= "pierna1.jpg"
                            imagen.onload = function(){
                                ctx.drawImage(imagen, 40, 15);
                            }
                        }
                            if(errores===6){
                                imagen.src= "pierna2.jpg"
                                imagen.onload = function(){
                                    ctx.drawImage(imagen, 40, 15);
                                }
                            }
                        }
            
        else{
            aciertos++;

        for(let i=0; i<juego.length;i++){
            let casilla = document.getElementById("letraP"+(i));
            if(miTecla.value === juego[i]){
            casilla.value = juego[i];
            }
        }  

    }
    if(aciertos===juego.length){
        alert("Ha ganado");
        this.Reiniciar();
    }
    if(errores===6){
        alert("Ha perdido");
        this.Reiniciar();
    }
}
    Reiniciar(){
    c=Math.round(Math.random()*8);     
    k=0;
    errores=0;
    aciertos=0;
    let division = document.getElementById("palabra");
    let teclado = document.getElementById("divTeclado");
        division.innerHTML ="";
        teclado.innerHTML ="";
    this.jugar();
    this.verificarLetra();
    }
}   
